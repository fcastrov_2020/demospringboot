package com.telefonica.motor.ofertas.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telefonica.motor.ofertas.entity.User;

@RestController()
@RequestMapping("/users")
public class UsuarioController {

	
	@GetMapping("/demo")
	public ResponseEntity<String> saludo() {
		return new ResponseEntity<>("Hello",HttpStatus.OK); 
	}
	
	@GetMapping("/list")
	public ResponseEntity<List<User>> listar(){
		List<User> userList = new ArrayList<>();
		User user = new User(1,"Jhon","Breña");
		User user2 = new User(2,"Cesar","Callao");
		User user3 = new User(3,"Fernndo","Surco");
		userList.add(user);
		userList.add(user2);
		userList.add(user3);
		return new ResponseEntity<>(userList,HttpStatus.OK);
	}
	
	@PostMapping("addUser")
	public ResponseEntity<User> addUsuario (@RequestBody User user) {
		return new ResponseEntity<>(user,HttpStatus.OK);
	}
	
	@PutMapping("editUser/{id}")
	public ResponseEntity<User> editUsuario(@RequestBody User user, @PathVariable int id){
		user.setId(id);
		return new ResponseEntity<>(user,HttpStatus.OK);
	}	
}
