package com.telefonica.motor.ofertas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MotorOfertasApplication {

	public static void main(String[] args) {
		SpringApplication.run(MotorOfertasApplication.class, args);
	}

}
