package com.telefonica.MotorOfertas;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.telefonica.motor.ofertas.MotorOfertasApplication;

@SpringBootTest(classes=MotorOfertasApplication.class)
class MotorOfertasApplicationTests {

	@Test
	void contextLoads() {
	}

}
