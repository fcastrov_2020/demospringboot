FROM openjdk:8-windowsservercore
LABEL maintainer="fcastrov@indracompany.com"
COPY target/MotorOfertas-0.0.1-SNAPSHOT.jar /MotorOfertas.jar
CMD ["java", "-jar", "MotorOfertas.jar"]